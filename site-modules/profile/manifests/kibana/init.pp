# @summary A short summary of the purpose of this class
#
# A description of what this class does
#
# @example
#   include kibana
class kibana {
  #need wget to 
  package { 'wget':
    ensure => present,
    provider => 'apt',
  }

  contain kibana::install
  contain kibana::config
  contain kibana::service

  Class['::kibana::install']
  -> Class['::kibana::config']
  -> Class['::kibana::service']
  }

