class profile::filebeat::filebeat_init (
String $packageName	= 'filebeat',
String $packageEnsure	= '7.9.2-linux-x86_64',
String $ip_kibana	= '192.168.180.130',
String $port_kibana	= '5601',
String $ip_logstash	= '192.168.180.124',
String $port_logstash	= '5044',
String $downloadSource	= 'https://artifacts.elastic.co/downloads/beats/filebeat/filebeat-7.9.2-linux-x86_64.tar.gz',

){
include profile::filebeat::linux_get_file
include profile::filebeat::linux_config
include profile::filebeat::linux_service


}
