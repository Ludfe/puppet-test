class profile::filebeat::linux_service {

# Make a service for linux.
# Dependent on ..get_file & conifg

	service {'filebeat':
		ensure	=> running,
		enable	=> true,
		status	=> '/usr/sbin/service filebeat status | grep "is running"',
	}

	file {'/etc/systemd/system/filebeat.service':
		ensure	=> present,	
		owner	=> 'root',
		group	=> 'root',
		mode	=> '0700',
		content	=> template('profile/service.erp'),
		notify	=> Service["filebeat"],
	}
}
