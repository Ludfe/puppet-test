class profile::filebeat::linux_config {

		$ip_kibana 	= $profile::filebeat::filebeat_init::ip_kibana
		$port_kibana	= $profile::filebeat::filebeat_init::port_kibana
		$ip_logstash	= $profile::filebeat::filebeat_init::ip_logstash
                $port_logstash  = $profile::filebeat::filebeat_init::port_logstash

	$filebeat_hash = { 
		'ip_kibana'	=> $ip_kibana,	
		'port_kibana'	=> $port_kibana, 
		'ip_logstash'	=> $ip_logstash,
		'port_logstash'	=> $port_logstash,

	} 

	file {'/opt/filebeat/archive/filebeat-7.9.2-linux-x86_64/filebeat.yml':
		ensure	=> present,
		owner	=> 'root',
		content	=> epp("profile/filebeat_linux.yaml.epp",$filebeat_hash),

	}

}
