class profile::filebeat::linux_get_file{

include ::archive

# Class gets the package using puppet-archive, and extracts it.

# Using facter to get "os family name".
if $osfamily == 'Debian' {
	$archiveName = "${profile::filebeat::filebeat_init::packageName}-${profile::filebeat::filebeat_init::packageEnsure}.tar.gz"

	file { [ '/opt/filebeat' , '/opt/filebeat/archive'] :
		ensure	=> directory,
	}

	archive { $profile::filebeat::filebeat_init::packageName:
		source		=> $profile::filebeat::filebeat_init::downloadSource,
		path		=> "/tmp/${archiveName}",
		temp_dir	=> '/tmp/',
		extract		=> true,
		extract_path	=> '/opt/filebeat/archive',
		creates		=> '/filebeat/archive',
		cleanup		=> true,
	}	
	
	# Creates sym link that is used for running filbeat.service.
	file {'/usr/local/bin/filebeat':
		ensure	=> link,
		target	=> "/opt/filebeat/archive/${profile::filebeat::filebeat_init::packageName}-${profile::filebeat::filebeat_init::packageEnsure}/filebeat",
		

	}
	}

}
