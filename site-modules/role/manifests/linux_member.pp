class role::linux_member {
    include ::profile::base_linux
    include ::profile::dns::client
    include ::profile::consul::client
}
